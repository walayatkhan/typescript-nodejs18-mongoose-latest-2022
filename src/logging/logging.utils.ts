import * as moment from 'moment';
import { getCurrentEnvironment } from '../config/constants';
import { MAXIMUM_DEPTH_FOR_LOGGING_DATA } from './logging.constants';

const getUserData = (res) => res?.req?.userData;
const restrictLengthyData = (data, depth = 0) => {
  if (depth > MAXIMUM_DEPTH_FOR_LOGGING_DATA)
    return `Data of Type ${typeof data}`;
  if (!data) return 'Null Or Undefined';
  const isArray = Array.isArray(data);
  const isTypeObject = typeof data === 'object';
  const isDate = data instanceof Date;
  const restricted = {};
  if (typeof data === 'object' && isArray) {
    return data.map((el) => restrictLengthyData(el, depth + 1));
  }
  if (isDate) return data.toString();
  if (isTypeObject) {
    Object.keys(data).forEach((k) => {
      if (typeof data[k] === 'object') {
        restricted[k] = restrictLengthyData(data[k], depth + 1);
      } else {
        restricted[k] = data[k];
      }
    });
    return restricted;
  }

  return data;
};
export const prepareErrorLog = (res, error) => {
  // To understand the logic behind the stack trace and
  // logic below, please see comments in prepareDebugLogs method
  // in this file
  const errorStackSplitted = error.stack?.split('\n');
  const calculatedLocation =
    errorStackSplitted?.length > 6
      ? `${errorStackSplitted[7]?.trim().split(' ')[2]}`
      : 'Unknown';
  const newLineSplit = error?.stack?.split('\n');
  const shortStackTrace =
    newLineSplit?.slice(0, 2) || 'Error stack not available';
  const functionName = { calculatedLocation, shortStackTrace };

  const { method } = res?.req || '';
  const endpoint = res?.req?.originalUrl;
  const requestId = res?.req?.uniqueRequestId || 'Unavailable';
  const userData = getUserData(res);
  const { message, statusCode, ...metaData } = error;
  const restrictedMetaData = restrictLengthyData(metaData);
  const environment = getCurrentEnvironment().toUpperCase();
  const timeOfError = moment().format('DD-MM-YYYY hh:mm');
  const { toNotLog = false } = error;
  return {
    requestId,
    userData,
    message,
    statusCode,
    restrictedMetaData,
    environment,
    timeOfError,
    method,
    endpoint,
    functionName,
    toNotLog,
  };
};
export const prepareOkLogs = (res, data) => {
  const { method } = res?.req || '';
  const endpoint = res?.req?.originalUrl;
  const requestId = res.req?.uniqueRequestId || 'Unavailable';
  const environment = getCurrentEnvironment().toUpperCase();
  const timeOfSuccess = moment().format('DD-MM-YYYY hh:mm');
  const {
    statusCode = 200,
    message = `No Display Message`,
    toNotLog = false,
  } = data;
  return {
    method,
    endpoint,
    requestId,
    environment,
    timeOfSuccess,
    statusCode,
    message,
    toNotLog,
  };
};
export const prepareDebugLogs = (label = '====DEBUGGING====') => {
  const dummyError = new Error();
  // length greater than 4 because we want to see which method logged warning
  // so 0 will be just showing that this is error stack<Useless info>, 1 will
  // be method which throw error<Useless info here because current method is
  // throwing temporary error > and 2 will be the method which called the error
  // throwing method<we call prepare logs method from logging service method so that
  // is also not useful>,3 will be the method which logged the warning<USEFUL> and
  // 4 will be the parent method of a method which logged the warning
  const canFindLogLocation = dummyError.stack.split('\n').length > 4;
  const functionName = canFindLogLocation
    ? `${dummyError.stack.split('\n')[3].trim().split(' ')[1]}--${
        dummyError.stack.split('\n')[4].trim().split(' ')[1]
      }`
    : 'Unknown';
  const environment = getCurrentEnvironment().toUpperCase();
  const timeOfLog = moment().format('DD-MM-YYYY hh:mm');
  return {
    functionName,
    environment,
    timeOfLog,
    label,
  };
};
export const prepareWarningLogs = (message: string) => {
  const environment = getCurrentEnvironment().toUpperCase();
  const timeOfWarning = moment().format('DD-MM-YYYY hh:mm');
  return {
    message: message.toUpperCase(),
    environment,
    timeOfWarning,
  };
};
