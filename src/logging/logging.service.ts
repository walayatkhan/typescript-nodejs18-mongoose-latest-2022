import { Response } from 'express';
import { createLogger } from 'winston';
import { ENVIRONMENT, getCurrentEnvironment } from '../config/constants';
import {
  prepareDebugLogs,
  prepareErrorLog,
  prepareOkLogs,
  prepareWarningLogs,
} from './logging.utils';
import { CONSOLE_TRANSPORTER, GOOGLE_TRANSPORTER } from './winston-google';

const getTransporters = (
  loggerType: 'ERROR' | 'SUCCESS' | 'WARN' | 'DEBUG',
) => {
  switch (getCurrentEnvironment()) {
    case ENVIRONMENT.PRODUCTION:
      return GOOGLE_TRANSPORTER[loggerType] || [];
    case ENVIRONMENT.STABLE:
      return [
        ...(GOOGLE_TRANSPORTER[loggerType] || []),
        ...(CONSOLE_TRANSPORTER[loggerType] || []),
      ];
    case ENVIRONMENT.DEVELOPMENT:
      return [
        ...(GOOGLE_TRANSPORTER[loggerType] || []),
        ...(CONSOLE_TRANSPORTER[loggerType] || []),
      ];
    // default transporter console so 'local' will also fall in this
    default:
      return CONSOLE_TRANSPORTER[loggerType] || [];
  }
};
class Logger {
  #errorLogger;

  #debugLogger;

  #successLogger;

  #warnLogger;

  constructor() {
    this.#errorLogger = createLogger({
      transports: getTransporters('ERROR'),
    });
    this.#successLogger = createLogger({
      transports: getTransporters('SUCCESS'),
    });
    this.#warnLogger = createLogger({
      transports: getTransporters('WARN'),
    });
    this.#debugLogger = createLogger({
      transports: getTransporters('DEBUG'),
    });
  }

  success(res: Response, data?: object) {
    const {
      method,
      endpoint,
      requestId,
      environment,
      timeOfSuccess,
      statusCode,
      message,
      toNotLog,
    } = prepareOkLogs(res, data);
    if (!toNotLog)
      this.#successLogger.info(
        message,
        JSON.parse(
          JSON.stringify({
            endpoint,
            requestId,
            environment,
            timeOfSuccess,
            statusCode,
            method,
          }),
        ),
      );
  }

  error(res: Response, error: any) {
    const {
      message,
      userData,
      restrictedMetaData,
      timeOfError,
      statusCode,
      environment,
      requestId,
      method,
      functionName,
      endpoint,
      toNotLog,
    } = prepareErrorLog(res, error);
    if (!toNotLog) {
      // Found when the request should be logged
      this.#errorLogger.error(
        message,
        JSON.parse(
          JSON.stringify({
            endpoint,
            requestId,
            environment,
            userData,
            restrictedMetaData,
            timeOfError,
            statusCode,
            method,
            functionName,
          }),
        ),
      );
    }
  }

  debug(data: any, labelText: string) {
    const { environment, functionName, label, timeOfLog } =
      prepareDebugLogs(labelText);
    this.#debugLogger.debug(
      labelText,
      JSON.parse(
        JSON.stringify({
          environment,
          functionName,
          label,
          timeOfLog,
          loggedData: data,
        }),
      ),
    );
  }

  warn(warningMessage: string, supportingData: any) {
    const { message, environment, timeOfWarning } =
      prepareWarningLogs(warningMessage);
    this.#warnLogger.warn(
      message,
      JSON.parse(
        JSON.stringify({
          timeOfWarning,
          environment,
          supportingData,
        }),
      ),
    );
  }
}
export default new Logger();
