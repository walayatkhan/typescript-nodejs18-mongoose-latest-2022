import { v4 as uRequestId } from 'uuid';

export const generateRequestId = (req, _res, next) => {
  req.uniqueRequestId = uRequestId();
  // generating unique request id to keep track of requests
  next();
};
