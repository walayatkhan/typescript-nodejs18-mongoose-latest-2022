import { format } from 'winston';

export const CONSOLE_MESSAGE_ERROR = format.printf(
  ({ message, endpoint, method, statusCode, environment, requestId }) =>
    `<${environment}> \n ${method} / [${statusCode}] - ${endpoint} - ${message} \n Request Id: ${requestId} \n Additional Info:`,
);
export const CONSOLE_MESSAGE_SUCCESS = format.printf(
  ({
    message,
    environment,
    timeOfSuccess,
    method,
    endpoint,
    statusCode,
    requestId,
  }) =>
    `\n \n  <${environment}> \n ${method} / [${statusCode}] - ${endpoint} - ${message}\n Request ID: ${requestId}\n Time Of Request : ${timeOfSuccess}`,
);
export const CONSOLE_MESSAGE_WARN = format.printf(
  ({ message }) => ` \n !!! ${message} \n `,
);
export const CONSOLE_MESSAGE_DEBUG = format.printf(
  ({ environment, timeOfLog, message, functionName }) =>
    `\n \n <${environment}> \n ${message} \n Logged in Function : ${functionName} \n At : ${timeOfLog}\n`,
);
export const META_PRETTY = format.prettyPrint();
