import { LoggingWinston } from '@google-cloud/logging-winston';
import { join } from 'path';
import { getCurrentEnvironment } from '../../config/constants';

const googleProjectConfig = {
  prefix: getCurrentEnvironment(),
  projectId: 'argon-rider-266718',
  keyFilename: join(__dirname, '../../hlp-1c55fe4b1e8d.json'),
};
const GOOGLE_TRANSPORTER = {
  ERROR: [
    new LoggingWinston({
      ...googleProjectConfig,
      level: 'error',
    }),
  ],
  SUCCESS: [
    new LoggingWinston({
      ...googleProjectConfig,
      level: 'info',
    }),
  ],
  WARN: [
    new LoggingWinston({
      ...googleProjectConfig,
      level: 'warn',
    }),
  ],
  DEBUG: [
    new LoggingWinston({
      ...googleProjectConfig,
      level: 'info',
    }),
  ],
};
export default GOOGLE_TRANSPORTER;
