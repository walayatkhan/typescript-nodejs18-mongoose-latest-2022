import { format, transports } from 'winston';
import {
  CONSOLE_MESSAGE_DEBUG,
  CONSOLE_MESSAGE_ERROR,
  CONSOLE_MESSAGE_SUCCESS,
  CONSOLE_MESSAGE_WARN,
  META_PRETTY,
} from './winston.formats';

const CONSOLE_TRANSPORTER = {
  ERROR: [
    new transports.Console({
      // colorize: true,
      format: format.combine(
        CONSOLE_MESSAGE_ERROR,
        format.colorize({ all: true }),
      ),
      level: 'error',
    }),
    new transports.Console({
      // colorize: true,
      format: format.combine(META_PRETTY, format.colorize({ all: true })),
      level: 'error',
    }),
  ],
  SUCCESS: [
    new transports.Console({
      // colorize: true,
      format: format.combine(
        CONSOLE_MESSAGE_SUCCESS,
        format.colorize({ all: true }),
      ),
      level: 'info',
    }),
  ],
  WARN: [
    new transports.Console({
      // colorize: true,
      format: format.combine(
        CONSOLE_MESSAGE_WARN,
        format.colorize({ all: true }),
      ),
      level: 'warn',
    }),
    new transports.Console({
      // colorize: true,
      format: format.combine(META_PRETTY, format.colorize({ all: true })),
      level: 'warn',
    }),
  ],
  DEBUG: [
    new transports.Console({
      // colorize: true,
      format: format.combine(
        CONSOLE_MESSAGE_DEBUG,
        format.colorize({ all: true }),
      ),
      level: 'debug',
    }),
    new transports.Console({
      // colorize: true,
      format: format.combine(META_PRETTY, format.colorize({ all: true })),
      level: 'debug',
    }),
  ],
};
export default CONSOLE_TRANSPORTER;
