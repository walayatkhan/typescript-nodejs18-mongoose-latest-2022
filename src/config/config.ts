import { ENVIRONMENT, getCurrentEnvironment } from './constants';
//import path from 'path';
import * as path from 'path';
export function getMongoConfig() {
  const { DB_DB, DB_USER, DB_PASS } = process.env;
  const env = getCurrentEnvironment();
  let dbUri;

  if ([ENVIRONMENT.STABLE, ENVIRONMENT.DEVELOPMENT].includes(env)) {
    dbUri = `mongodb://${process.env.DB_URL}`;
    return {
      uri: dbUri,
      options: {
        dbName: DB_DB,
        user: DB_USER,
        pass: encodeURIComponent(DB_PASS),
        authSource: 'admin',
        replicaSet: 'rs0',
      },
    };
  }

  /**
   * Production Environment - Also using Replica Set
   */
  if (env === ENVIRONMENT.PRODUCTION) {
    let uri = '';
    let options = {};
    const certFile = path.join(__dirname, process.env.DB_CA);
    options = {
      ssl: true,
      sslValidate: true,
      sslCA: certFile,
      dbName: DB_DB,
    };
    uri = `mongodb://${DB_USER}:${encodeURIComponent(DB_PASS)}@${
      process.env.DB_URL
    }/admin?replicaSet=Hyper-Protect-MongoDB`;

    return { uri, options };
  }

  const uri = `mongodb://${process.env.DB_LOCAL_COMPUTER_NAME}:27017,${process.env.DB_LOCAL_COMPUTER_NAME}:27018,${process.env.DB_LOCAL_COMPUTER_NAME}:27019`;
  return { uri, options: { dbName: 'hlp-local', replicaSet: 'rs' } };
}
