export const ENVIRONMENT = {
  LOCAL: 'local',
  STABLE: 'stable',
  DEVELOPMENT: 'development',
  PRODUCTION: 'production',
};

export const getCurrentEnvironment = () =>
  process.env.ENVIRONMENT?.toLowerCase() || ENVIRONMENT['DEVELOPMENT'];
