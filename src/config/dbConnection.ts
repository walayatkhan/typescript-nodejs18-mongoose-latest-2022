import mongoose from 'mongoose';
import { getMongoConfig } from './config';

const connectDB = async () => {
  const { options, uri } = getMongoConfig();
  const dbOptions = {
    ...options,
    autoIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
  try {
    await mongoose.connect(uri, dbOptions);
    console.log(`Successfully connected mongodb: ${uri}}`);
  } catch (err) {
    console.log(`Database error`, err);
  }
};

export default connectDB;
