import 'dotenv/config';
import * as express from 'express';
import connectDB from './config/dbConnection';
import swaggerDocs from './routes/apidocs.route';
import routes from './routes';
import { CONSOLE_COLOR } from './utils/constant';

const app = express();
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || '2022';

async function startServer() {
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json({ limit: '10mb' }));
  app.disable('x-powered-by');
  app.use('/api', routes);
  app.use('/api-swagger', swaggerDocs);
  app.get('/health', (_req, res) => {
    res.send('Server is running.');
  });
  await connectDB();
  app.listen(port, () => {
    console.log(
      CONSOLE_COLOR.cyan,
      `Server Running on HOST:  ${host} PORT ${port}`,
    );
  });
}

startServer();
