import { isBoolean } from 'lodash';
import validator from 'validator';
import mongoose from 'mongoose';
import { BadRequestError } from '../../error_handling/error_classes';

export const isEmail = (email: string, isOptional = false) => {
  if (
    (email && validator.isEmail(email)) ||
    (isOptional && (!email || validator.isEmail(email)))
  )
    return true;
  throw new BadRequestError('Email is invalid', { time: new Date(), email });
};

export const isMongoId = (id: number, isOptional = false) => {
  if (
    (id && mongoose.isValidObjectId(id)) ||
    (isOptional && (!id || mongoose.isValidObjectId(id)))
  )
    return true;
  throw new BadRequestError('Id is invalid', { time: new Date(), id });
};

export const isNotEmpty = (key: any, val: any) => {
  if (
    val === undefined ||
    val === null ||
    (typeof val === 'string' && validator.isEmpty(val))
  )
    throw new BadRequestError(`${key} should be provided`);
  return true;
};
export const isAlphabets = (key: any, val: any, isOptional = false) => {
  if (
    (val && typeof val === 'string' && validator.isAlpha(val)) ||
    (isOptional &&
      (!val || (typeof val === 'string' && validator.isAlpha(val))))
  )
    return true;
  throw new BadRequestError(
    `Please provide correct value for ${key}, Only alphabets are allowed`,
  );
};
export const isAlphabetsWithOptionalSpaces = (
  key: any,
  val: any,
  isOptional = false,
) => {
  if (
    (val &&
      typeof val === 'string' &&
      validator.isAlpha(val.replace(/ /g, ''))) ||
    (isOptional &&
      (!val ||
        (typeof val === 'string' && validator.isAlpha(val.replace(/ /g, '')))))
  )
    return true;
  throw new BadRequestError(
    `Please provide correct value for ${key}, Only alphabets are with optional spaces allowed`,
  );
};
export const isAlphaNumericWithOptionalSpaces = (
  key: any,
  val: any,
  isOptional = false,
) => {
  if (
    (val &&
      typeof val === 'string' &&
      validator.isAlphanumeric(val.replace(/ /g, ''))) ||
    (isOptional &&
      (!val ||
        (typeof val === 'string' &&
          validator.isAlphanumeric(val.replace(/ /g, '')))))
  )
    return true;
  throw new BadRequestError(
    `Please provide correct value for ${key}, Only alphabets with optional spaces allowed`,
  );
};
export const isAlphanumeric = (key: any, val: any, isOptional = false) => {
  if (
    (val && typeof val === 'string' && validator.isAlphanumeric(val)) ||
    (isOptional &&
      (!val || (typeof val === 'string' && validator.isAlphanumeric(val))))
  )
    return true;
  throw new BadRequestError(
    `Please provide correct value for ${key}, Only alphabets and numbers allowed`,
  );
};
/**
 *
 * @param {String} key
 * @param {Number} val
 */
export const isNumeric = (key: any, val: any, isOptional = false) => {
  if (
    typeof val === 'number' ||
    (isOptional && (!val || typeof val === 'number'))
  )
    return true;
  throw new BadRequestError(
    `Please provide correct value for ${key}, Only numbers allowed`,
  );
};
export const isBooleanType = (key: any, val: any, isOptional = false) => {
  if (isBoolean(val) || (isOptional && (val === undefined || isBoolean(val))))
    return true;
  throw new BadRequestError(`Please provide correct value for ${key}`);
};
export const haveNoMaliciousField = (
  fieldsArr: any,
  allowedUpdateFields: any,
) => {
  const diff = fieldsArr.filter(
    (field: any) => !allowedUpdateFields.includes(field),
  );
  if (diff.length)
    throw new BadRequestError(`Malformed Data given, [${diff.toString()}]`);
  return true;
};
