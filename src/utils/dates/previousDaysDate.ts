import * as moment from 'moment';
// previous days date
const previousDaysDate = (days: number) =>
  moment().subtract(days, 'days').toDate();

export default previousDaysDate;
