import { Request } from 'express';
export type AppRequest = Request & { uniqueRequestId: string };
