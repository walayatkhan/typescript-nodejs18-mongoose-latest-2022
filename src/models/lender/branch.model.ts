import mongoose from 'mongoose';
import IBranch from '../../interfaces/branch.interface';

const { Schema } = mongoose;

const BranchAddressSchema = new Schema({
  state: {
    type: String,
    required: true,
  },
  city: {
    type: String,
    required: true,
  },
  streetAddress: {
    type: String,
    required: true,
  },
});
const BranchSchema = new Schema(
  {
    branchAdminUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Lender_User',
    },
    corporation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Lender_Corporation',
      required: true,
    },
    statesOperatesIn: {
      type: [
        {
          type: String,
          validate: /^[A-Z]{2}$/i,
          uppercase: true,
        },
      ],
      default: [],
    },
    loanOfficers: {
      type: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Lender_LoanOfficer',
        },
      ],
      default: [],
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    branchNMLSNumber: {
      type: String,
      required: true,
      unique: true,
      sparse: true,
    },
    branchAddress: {
      type: BranchAddressSchema,
      required: false,
    },
    branchPhoneNumber: {
      type: String,
      required: false,
    },
    receiveReferrals: {
      type: Boolean,
      default: true,
    },
    assignAlgorithm: {
      type: Boolean,
      default: true,
    },
    branchName: {
      type: String,
      required: false,
    },
  },
  { timestamps: true },
);

const LenderBranch = mongoose.model<IBranch>('Lender_Branch', BranchSchema);

export default LenderBranch;
