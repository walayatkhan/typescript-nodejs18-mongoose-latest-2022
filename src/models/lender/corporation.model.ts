import mongoose from 'mongoose';
import ICorporation from '../../interfaces/corporation.interface';

const { Schema } = mongoose;

const CorporationSchema = new Schema(
  {
    adminUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Lender_User',
    },
    overlays: {
      type: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Overlay',
        },
      ],
      default: [],
    },
    branches: {
      type: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'Lender_Branch',
        },
      ],
      default: [],
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    corporationNMLSNumber: {
      type: String,
      required: true,
      unique: true,
      sparse: true,
    },
    corporationLogo: {
      type: {
        data: Buffer,
        contentType: String,
      },
      required: false,
    },
    branchCanModifyOverlay: {
      type: Boolean,
      default: false,
    },
    organizationId: {
      type: String,
      required: false,
    },
    preAppPolicyId: {
      type: String,
      required: false,
    },
    corporationName: {
      type: String,
      required: true,
    },
  },
  { timestamps: true },
);

const LenderCorporation = mongoose.model<ICorporation>(
  'Lender_Corporation',
  CorporationSchema,
);

export default LenderCorporation;
