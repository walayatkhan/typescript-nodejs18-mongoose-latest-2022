import mongoose from 'mongoose';
import { IUser } from 'src/interfaces/user.interface';
import { isEmail } from '../../utils/validations/general';

const { Schema } = mongoose;

/*
This schema is shared between all users of lender portal. It has common values. The idea is to generalize all entities into one entity user
This will make it easier for log/signup/invite with single point of contact and also makes it easier to fetch data for each user.
*/
const UserSchema = new Schema(
  {
    firstName: {
      validate: /[a-zA-Z]/,
      type: String,
      required: true,
    },
    lastName: {
      validate: /[a-zA-Z]/,
      type: String,
      required: true,
    },
    title: {
      type: String,
      default: undefined,
    },
    profilePicture: {
      type: {
        data: Buffer,
        contentType: String,
      },
      default: undefined,
    },
    email: {
      validate: [isEmail, 'Email provided is not valid'],
      type: String,
      required: true,
      unique: true,
      sparse: true,
    },
    phoneNumber: {
      type: String,
      default: undefined,
    },
    password: {
      type: String,
      required: true,
      select: false,
    },
    invitationAccepted: {
      type: Boolean,
      default: false,
    },
    isActive: {
      type: Boolean,
      default: true,
    },
    userType: {
      type: [
        {
          type: String,
          uppercase: true,
          required: true,
          enum: ['LOAN-OFFICER', 'BRANCH-ADMIN', 'CORPORATION-ADMIN'],
        },
      ],
      required: true,
    },
  },
  { timestamps: true },
);

const User = mongoose.model<IUser>('Lender_User', UserSchema);

export default User;
