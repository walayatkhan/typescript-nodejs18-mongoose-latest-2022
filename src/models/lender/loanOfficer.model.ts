import mongoose from 'mongoose';
import ILoanOfficer from '../../interfaces/invite.interface';
const { Schema } = mongoose;

const LoanOfficerSchema = new Schema(
  {
    branch: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Lender_Branch',
      required: true,
    },
    loanOfficerUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Lender_User',
    },
    loanOfficernmlsNumber: {
      type: String,
      required: true,
      unique: true,
      sparse: true,
    },
    statesOperatesIn: {
      type: [
        {
          type: String,
          validate: /^[A-Z]{2}$/i,
          uppercase: true,
        },
      ],
      default: [],
    },
  },
  { timestamps: true },
);

const LenderLoanOfficer = mongoose.model<ILoanOfficer>(
  'Lender_LoanOfficer',
  LoanOfficerSchema,
);

export default LenderLoanOfficer;
