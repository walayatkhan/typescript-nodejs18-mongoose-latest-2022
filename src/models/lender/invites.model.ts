import mongoose from 'mongoose';
import IInvites from '../../interfaces/invite.interface';
import { validateEmail } from '../../utils/validations/isEmail';

const { Schema } = mongoose;

const InvitesSchema = new Schema(
  {
    inviteeEmail: {
      type: String,
      validate: [validateEmail, 'Email provided is not valid'],
      lowercase: true,
      trim: true,
      required: true,
      unique: true,
    },
    // roleInvitedFor is array which i only for the reason that if in future we invite user for multiple roles
    // simultaneously, we can do it. But for now it is only being used as a string in implementation of model
    roleInvitedFor: {
      type: String,
      enum: ['BRANCH-ADMIN', 'LOAN-OFFICER', 'CORPORATION-ADMIN'],
      uppercase: true,
      required: true,
    },
    parentDocument: {
      type: mongoose.Types.ObjectId,
      required: true,
    },
  },
  { timestamps: true },
);

const Invites = mongoose.model<IInvites>('Lender_Invites', InvitesSchema);

export default Invites;
