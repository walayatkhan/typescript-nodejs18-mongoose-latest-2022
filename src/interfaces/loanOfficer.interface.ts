import { ObjectId } from 'mongoose';

export default interface ILoanOfficer {
  branch: ObjectId;
  loanOfficerNMLSNumber: string;
  statesOperatesIn: string[];
  loanOfficerUser: ObjectId;
  createdAt: Date;
  updatedAt: Date;
}
