export interface IUser {
  firstName: string;
  lastName: string;
  title: string;
  profilePicture?: string;
  email: string;
  phoneNumber: string;
  password: string;
  invitationAccepted: boolean;
  isActive?: boolean;
  userType: string;
  timestamps: boolean;
}
