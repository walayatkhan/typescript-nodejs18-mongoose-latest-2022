import { ObjectId } from 'mongoose';

export default interface ICorporation {
  overlays: [];
  branches: [];
  isActive: boolean;
  corporationNMLSNumber: string;
  branchCanModifyOverlay: boolean;
  organizationId: ObjectId;
  preAppPolicyId: string;
  corporationName: string;
  createdAt: Date;
  updatedAt: Date;
}
