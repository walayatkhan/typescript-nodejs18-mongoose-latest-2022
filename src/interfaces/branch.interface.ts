import { ObjectId } from 'mongoose';

export default interface IBranch {
  branchAdminUser: ObjectId;
  corporation: ObjectId;
  stateOperatesIn: string[];
  loanOfficers: ObjectId[];
  isActive: boolean;
  branchNMLSNumber: string;
  branchAddress: Record<string, unknown>;
  receiveReferrals: boolean;
  assignAlgorithm: boolean;
  branchName: string;
  createdAt: Date;
  updatedAt: Date;
}
