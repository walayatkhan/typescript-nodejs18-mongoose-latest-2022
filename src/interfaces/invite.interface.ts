import { ObjectId } from 'mongoose';

export default interface IInvites {
  _id: ObjectId;
  inviteeEmail: string;
  roleInvitedFor: string;
  ParentDocument: ObjectId;
  createdAt: Date;
  updatedAt: Date;
}
