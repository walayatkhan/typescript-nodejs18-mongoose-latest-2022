import { Response } from 'express';
import { Error as MongooseError } from 'mongoose';
import { ApplicationError } from '../error_classes/Base';
import getMongooseError from './mongoose_error_handler';
import loggingService from '../../logging/logging.service';
import { AppRequest } from '../../utils/types/index';

// eslint-disable-next-line no-unused-vars
const handleErrors: any = (
  err: any,
  req: AppRequest,
  res: Response,
  // _next: any,
) => {
  const alwaysToSend = {
    success: false,
    requestId: req.uniqueRequestId,
  };
  loggingService.error(res, err);
  const mError: any = MongooseError;
  if (err instanceof mError || [11000].includes(err.code)) {
    // Mongoose errors and mongo native errors are different hence using code as well
    const { message, statusCode } = getMongooseError(err);
    return res.status(statusCode).json({ message, ...alwaysToSend });
  }

  if (err instanceof ApplicationError) {
    const { message, statusCode, additionalData = {} } = err;
    return res
      .status(statusCode)
      .json({ message, ...alwaysToSend, ...additionalData });
  }

  return res
    .status(500)
    .json({ message: err.message || 'Unknown', ...alwaysToSend });
};

export default handleErrors;
