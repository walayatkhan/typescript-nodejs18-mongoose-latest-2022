import { Error } from 'mongoose';
import { RETURN_CODE } from '../../utils/constant';

// TODO complete check of all errors and send appropriate response
export default (err) => {
  if (err instanceof Error.ValidationError) {
    return {
      message: `Please provide appropriate values for ${Object.keys(
        err.errors,
      ).toString()}`,
      statusCode: RETURN_CODE.BAD_REQUEST,
    };
  }
  if (err.code === 11000) {
    // for mongo native errors, mongoose doesn't know them. we have to provide error codes of native errors for mongodb
    return {
      message: `Record already exists against field ${
        err.message.split('dup key:').length
          ? err.message.split('dup key:')[1]
          : err.message
      }`,
      statusCode: RETURN_CODE.CONFLICT,
    };
  }
  return {
    message: `DB Error ${err.message}`,
    statusCode: RETURN_CODE.INTERNAL_SERVER,
  };
};
