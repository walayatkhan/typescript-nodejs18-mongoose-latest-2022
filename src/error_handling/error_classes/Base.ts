// Here is the base error classes to extend from

class ApplicationError extends Error {
  [x: string]: any;
  // Additional data is the data to be sent to front end
  // in response payload, other than success:false,
  // message and status code. This can be any
  // additional data which is needed to tell front end
  // something in addition to message, for example see
  // generate asset report controller in plaid controller
  // in catch block
  /**
   *
   * @param {String} message
   * @param {{toNotLog?:Boolean,[any]:any}} loggingData
   * @param {{[any]:any}} additionalData
   */
  constructor(message: string, additionalData: any, loggingData: any) {
    super(message);
    this.additionalData = additionalData;
    for (const [key, value] of Object.entries(loggingData)) {
      this[key] = value;
    }
  }

  get name() {
    return this.constructor.name;
  }
}
export { ApplicationError };
