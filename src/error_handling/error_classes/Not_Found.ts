import { RETURN_CODE } from '../../utils/constant';
import { ApplicationError } from './Base';

export default class NotFoundError extends ApplicationError {
  /**
   *
   * @param {String} message
   * @param {{toNotLog?:Boolean,[any]:any}} loggingData
   * @param {{[any]:any}} additionalData
   */
  constructor(message: string, loggingData = {}, additionalData = undefined) {
    // Additional data is the data to be sent to front end
    // in response payload, other than success:false,
    // message and status code. This can be any
    // additional data which is needed to tell front end
    // something in addition to message, for example see
    // generate asset report controller in plaid controller
    // in catch block
    /**
     *
     * @param {String} message
     * @param {{[any]:any}} additionalData
     */
    super(message, additionalData, loggingData);
    this.statusCode = RETURN_CODE.NOT_FOUND;
  }
}
