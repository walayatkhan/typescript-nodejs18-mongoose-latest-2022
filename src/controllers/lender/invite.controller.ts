import { Response } from 'express';
import IInvites from '../../interfaces/invite.interface';
import { ReS } from '../../response_handling/response_handler';
import * as Invites from '../../services/lender/invites/invites.service';
import { RETURN_CODE } from '../../utils/constant';

export const getInvites = async (_req, res: Response) => {
  const invites: IInvites[] = await Invites.getAll();
  return ReS(res, { data: invites || [] }, RETURN_CODE.SUCCESS);
};
