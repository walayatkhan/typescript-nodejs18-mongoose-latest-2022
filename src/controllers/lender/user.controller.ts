import { Request, Response } from 'express';
import { RETURN_CODE } from '../../utils/constant';
import { ReS } from '../../response_handling/response_handler';
import { User } from '../../services/index';
import { IUser } from '../../interfaces/user.interface';

export const getAllUser = async (_req: Request, res: Response) => {
  const users: IUser[] = await User.getAll();
  return ReS(res, { data: users || [] }, RETURN_CODE.SUCCESS);
};
