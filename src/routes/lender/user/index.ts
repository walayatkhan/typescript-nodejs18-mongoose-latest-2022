import * as express from 'express';
const router = express();

import * as userControllers from '../../../controllers/lender/user.controller';

router.get('/', userControllers.getAllUser);

export default router;
