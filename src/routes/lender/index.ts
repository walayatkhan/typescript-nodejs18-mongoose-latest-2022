import * as express from 'express';
const router = express();

import userRoutes from './user/index';
import inviteRoutes from './invite/index';

router.use('/user', userRoutes);
router.use('/invite', inviteRoutes);

export default router;
