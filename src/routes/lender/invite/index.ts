import * as express from 'express';
const router = express();
import { getInvites } from '../../../controllers/lender/invite.controller';

router.get('/', getInvites);

export default router;
