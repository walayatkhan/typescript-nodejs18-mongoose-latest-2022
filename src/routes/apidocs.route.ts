import * as swaggerUi from 'swagger-ui-express';
import * as path from 'path';
import * as YAML from 'yamljs';

const yamlPath = path.resolve('./apidocs/api_docs.yaml');
const swaggerDocument = YAML.load(yamlPath);

import * as express from 'express';
const router = express.Router();

router.use('/', swaggerUi.serve);
router.get('/', swaggerUi.setup(swaggerDocument));

export default router;
