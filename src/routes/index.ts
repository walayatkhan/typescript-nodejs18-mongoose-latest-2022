import * as express from 'express';
const router = express();
import LenderRoutes from './lender/index';

router.use('/lender', LenderRoutes);

export default router;
