import IInvites from '../../../interfaces/invite.interface';
import Invites from '../../../models/lender/invites.model';

export const getAll = async (options = { population: [] }) => {
  const invites: IInvites[] = await Invites.find()
    .populate(options.population || [])
    .exec();
  return invites;
};

export const getMany = async (findQuery, options = { population: [] }) => {
  // get many documents based on query
  const invites: IInvites[] = await Invites.find(findQuery)
    .populate(options.population || [])
    .exec();
  return invites;
};
