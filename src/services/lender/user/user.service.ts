import { IUser } from '../../../interfaces/user.interface';
import User from '../../../models/lender/user.model';

export const getAll = async (options = { population: [] }) => {
  const users: IUser[] = await User.find()
    .populate(options.population || [])
    .exec();
  return users;
};
