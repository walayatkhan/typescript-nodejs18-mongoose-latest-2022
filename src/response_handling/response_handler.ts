import { RETURN_CODE } from '../utils/constant';
import loggingService from '../logging/logging.service';

/**
 * Wrapper for success or non-error response
 * @param {import('express').Response} res
 * @param {{toNotLog:boolean,[string]:any}} data
 * @param {number} code
 * @returns
 */
export const ReS = (res: any, data: object, code = RETURN_CODE.SUCCESS) => {
  loggingService.success(res, { ...data, statusCode: code });
  let toSend = { success: true, requestId: res.req.uniqueRequestId };
  if (typeof data === 'object') {
    toSend = Object.assign(toSend, data); // merge the objects
  }
  return res.status(code).json(toSend);
};
